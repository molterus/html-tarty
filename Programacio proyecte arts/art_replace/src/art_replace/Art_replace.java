package art_replace;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.*;
import java.awt.event.*;
import javax.imageio.ImageIO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.nio.channels.FileChannel;
import java.nio.file.*;

public class Art_replace {
    public static String ext;
    public static int MAX_FILE=30;
    public static String dades[]= new String[2];
    public static String bak = "bak";

    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String c_projecte,c_imatges;
        String arr_img_prj[] = new String [MAX_FILE];
        String arr_img_mod[] = new String [MAX_FILE];
        
        demanar_dades (dades);
        c_projecte=dades[0];
        c_imatges=dades[1];
        
        File fp = new File(c_projecte);
        recorrer_dir_rec (fp,arr_img_prj);
        //System.out.println (Arrays.toString(arr_img_prj));
        File fi = new File(c_imatges);
        recorrer_dir_rec (fi,arr_img_mod);
        //System.out.println (Arrays.toString(arr_img_mod));
        Mostrar_informacio (arr_img_prj,arr_img_mod);
        menu (arr_img_prj,arr_img_mod);
    }
    //S'HA DE POSAR MES ORDENAT
    private static void Mostrar_informacio(String arr_img_proj[],String arr_img_mod[]){
        int i ;
        String espais= " ";
        System.out.println ("==========================================");
        System.out.println ("Imatges del projecte");
        System.out.println ("==========================================");           
        for (i = 0;i<arr_img_proj.length;i++){
        espais = calcula_espais(arr_img_proj[i]!=null?arr_img_proj[i].length():4);
        System.out.println(i+ " - " + arr_img_proj[i]);
        espais = " ";
        if (arr_img_proj[i]==null)
            break;
        }
        System.out.println ("==========================================");
        System.out.println ("Imatges de substitució");
        System.out.println ("==========================================");
        for (i = 0;i<arr_img_proj.length;i++){
        espais = calcula_espais(arr_img_proj[i]!=null?arr_img_proj[i].length():4);
        System.out.println(i+ " - " + arr_img_mod[i]);
        espais = " ";
        if (arr_img_mod[i]==null)
            break;
        }
    }

    private static String calcula_espais (int num){
            int i;
            String espai = " ";
            for (i =num;i<=23;i++)
                espai = espai + " ";
            return espai;
    }
    private static void demanar_dades (String dades[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introdueix la carpeta del projecte: ");
        dades[0] = sc.nextLine();
        System.out.print("Introdueix la carpeta d'imatges: ");
        dades[1] = sc.nextLine();
        System.out.print("Introdueix la extensió de les imatges: ");
        ext = sc.nextLine();
    }
    
    private static void recorrer_dir_rec (File f, String arr_img[]) throws IOException{
        File[] fitxers = f.listFiles();
        for (int x=0;x<fitxers.length;x++){                    
            if (fitxers[x].isFile())
                guardar_imatges_dir(fitxers[x],arr_img);
            else if (fitxers[x].isDirectory())
                recorrer_dir_rec(fitxers[x],arr_img);
        }              
    }    
   
    private static void guardar_imatges_dir (File f, String arr_img[]){
        if (f.getName().indexOf("." + ext)!=-1){
            int i=0;
            while (arr_img[i]!=null)
                i++;
            arr_img[i]=f.getPath();
        }
    }
    private static void guardar_imatges_fitx (File f, String arr_img[],String nom_fitxer) throws FileNotFoundException, IOException{
        int i=0, e=0;
        
        //Guarda les recerques
        /*while (arr_img[e]!=null)
            e++; */
        //Funcio per a llegir linees de fitxers, fitxer declarat
        BufferedReader rli = new BufferedReader(new FileReader(f));
        String linia = rli.readLine();
        //Mentre hi hagin linies, s'aniran llegin
        while(linia != null){
            e++;
            if (linia.contains(nom_fitxer)){
                arr_img[i]= "Linea: '" + e + "' amb el text: '" + linia + "' apareix en el fitxer: '" + f.getName() + "'.";
                i++;
            }
            //Continua buscan despres de trobar una coincidencia
            linia = rli.readLine();
        }
    }

    private static String recupera_nom_fitxer(String linia) {
        int index=0;
        String nom_file="";
        index = linia.indexOf("." + ext,index);
            while (linia.charAt(index)!=' ' && linia.charAt(index)!='\\'&& linia.charAt(index)!='/'){
                    nom_file= linia.charAt(index) + nom_file;
                    index = index-1;
            }
            return nom_file + ext;
    }
    
    public static void menu (String array_img[],String array_imatge_mod[]) throws IOException {
        int opcions=0;
        String array_line[] = new String [100];
            while (opcions <4) {
            System.out.println("*********************************");
            System.out.println("*1-Informacio de la imatge.******");
            System.out.println("*2-Canviar imatge per una teva.**");
            System.out.println("*3-Sortir del programa.**********");
            System.out.println("*********************************");

            Scanner menux = new Scanner (System.in);
            System.out.println("Introdueix una opció:");
            opcions = menux.nextInt();
                switch(opcions){
                    case 1:{info(array_img,array_line);break;}
                    case 2:{canvi(array_img,array_imatge_mod);break;}
                    case 3:{surt();break;}
                }
            }
    }
    
    public static void surt(){
        System.out.println("Has sortit del programa.");
        System.exit(0);

    }

    public static void info(String array_imatge[],String array_line[]) throws IOException {
            String nom_fitxer;
            System.out.println("============================================");
            Scanner menux = new Scanner (System.in);
            System.out.println("Introdueix el fitxer:");
            int file_num = menux.nextInt();
            //
            nom_fitxer = recupera_nom_fitxer(array_imatge[file_num]);
            File fp = new File(dades[0]);
            recorrer_dir_rec2(fp,array_line,nom_fitxer);
            //Mostra on es troba el fitxer i la linea
            int i=0;
            while (array_line[i]!=null) {
                System.out.println(array_line[i]);
                i++;
            }
            
        }
    //S'HA DE SOLUCIONAR EL NULL
    private static void recorrer_dir_rec2 (File f, String arr_img[], String nom_fitxer) throws IOException{
    File[] fitxers = f.listFiles();
    for (int x=0;x<fitxers.length;x++){
            if (fitxers[x].isFile())
                guardar_imatges_fitx(fitxers[x],arr_img,nom_fitxer);
            else if (fitxers[x].isDirectory())
                recorrer_dir_rec2(fitxers[x],arr_img,nom_fitxer);
        }
    }
    public static void canvi(String array_imatge[],String array_imatge_mod[]) throws IOException{
        
        Scanner canvi = new Scanner (System.in);
        System.out.println("Introdueix el numero de la imatge que vols canviar: ");
            int file_num = canvi.nextInt();
            String nom_fitxer = recupera_nom_fitxer(array_imatge[file_num]);
            File old = new File(array_imatge[file_num]);
            //Amb això creem una variable per a poder fer una còpia de seguretat del fitxer
            File copia_bak = new File (old + "." + bak);
            File old1 = new File(array_imatge[file_num]);
            
        System.out.println("Introdueix el numero de la imatge de la teva carpeta");
            int file_mod = canvi.nextInt();
            String nom_fitxer_mod = recupera_nom_fitxer(array_imatge_mod[file_mod]);
            File newy = new File(array_imatge_mod[file_mod]);
            //Fem la còpia de seguretat del fitxer
            old.renameTo(copia_bak);
            
            //Desprès de fer la còpia, fem el traspas de fitxer
            try {
                FileChannel in = (new FileInputStream(newy)).getChannel();
                FileChannel out = (new FileOutputStream(old)).getChannel();
                in.transferTo(0, newy.length(), out);
                in.close();
                out.close();
            }
            //Si no es poguès fer, donaria un error
            catch(Exception e) {
                System.out.println(e);
            }
        System.out.println("==================================================");
        System.out.println(" ");
        System.out.println("La imatge '" + old + "' ha estat substituida per '" + newy + "'");
        System.out.println(" ");
        System.out.println("==================================================");
    }
/*///////////////////////////////////////////////
    +++++++++++++++SEPARACIO++++++++++++++++++++
    ///////////////////////////////////////////
*/   
    //Altre mètode de copia
    /*public static void copiar (String nom_fitxer, String nom_fitxer_mod) throws IOException {
        Path desde = Paths.get(nom_fitxer);
        Path capa = Paths.get(nom_fitxer_mod);
        Files.copy(desde, capa);
    }*/
/*///////////////////////////////////////////////
    +++++++++++++++SEPARACIO++++++++++++++++++++
    ///////////////////////////////////////////
*/
    //Mètode per redimensionar
/*   class redimensiona extends Frame {
        Image imgmod, image1;
        public redimensiona(){
            this.setSize(300,150);
            imgmod = Toolkit.getDefaultToolkit().getImage(ext);
        }
    }*/
/*///////////////////////////////////////////////
    +++++++++++++++SEPARACIO++++++++++++++++++++
    ///////////////////////////////////////////
*/
}