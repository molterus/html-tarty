/**
 * Autor: Jason Guatatoca
 * Ins. Cirvianum de Torello - 2014-2015
 * 
 */

//variables globales
var listaObjetosCelda = [];//llista d'objectes cel-la
var canvasA = canvasB = false; //canvas auxiliars
var numA = numB = -1;//numero de canvas clickats
var winner = false; //si ja ha guanyat
var contador = 0;// contador de parelles

/**
 * INICIALITZACIÓ
 * @returns {undefined}
 */
window.onload = function() {
    init();
};

function init() {
    crearArrayDeImagenes();
    listaObjetosCelda.desordenar();
}

/**
 * MÈTODE PER CREAR LES CEL.LES 
 * @returns {undefined}
 */
function crearArrayDeImagenes() {
    var prefijo = "images/";
    var i, k;
    for (i = 1, k = 0; i < 7; i++, k++) {
        listaObjetosCelda[k] = new Celda(i, prefijo + i + ".png");
        listaObjetosCelda[++k] = new Celda(i, prefijo + i + ".png");
    }
}

/**
 * MÈTODE PER ORDENAR ALEATORIAMENT
 * @returns {undefined}
 */
Array.prototype.desordenar = function() {
    this.sort(desordena);
};

/**
 * MÈTODE DEFINIT PER QUE EL SORT PUGUI FER SERVIR PER ORDENAR
 * @param {type} a
 * @param {type} b
 * @returns {Number}
 */
function desordena(a, b) {
    return 0.5 - Math.random();
}

/**
 * MÈTODE QUE S'EXECUTA QUAN UN CANVAS ES CLICKAT
 * @param {type} n Num del canvas clickat
 * @returns {undefined} 
 */
function canvasClicked(n) {
    if (!canvasA || !canvasB) {//si aun no estan abiertas 2 caras
        var elCanvas = "canvas" + n;
        var c = document.getElementById(elCanvas);
        c.style.background = "transparent";//per q no aparegui el fons creat per defecte
        c.removeAttribute("onclick");//trec la funció onclick perque no es pugui clickar en el mateix canvas dues vegades

        cargarImagen(c, listaObjetosCelda[n - 1].imagen);//mostro la imatge en el canvas

        if (!canvasA) {//si es el primer canvas actiu
            canvasA = true;
            numA = n - 1;
        } else {//si es el segon canvas actiu
            canvasB = true;
            numB = n - 1;
            evaluar();//si ja es el segon canvas actiu, llavors es hora de fer les comparacions per coneixer si son o no igual les imatges
        }

    }


}

/**
 * Mètode per carregar una imatge a un canvas tots dos passats per paràmetre
 * @param {type} c Canvas
 * @param {type} url adreça de l'imatge
 * @returns {undefined}
 */
function cargarImagen(c, url) {
    var ctx = c.getContext("2d");
    var s = getComputedStyle(c);
    var w = s.width;
    var h = s.height;
    var width = c.width = w.split("px")[0];
    var height = c.height = h.split("px")[0];
    var img = new Image();
    img.src = url;
    img.onload = function() {
        ctx.drawImage(img, 0, 0, width, height);
    };
}

/**
 * Métode per evaluar si dos canvas tenen la mateixa imatge 
 * @returns {undefined}
 */
function evaluar() {
    var msg = "";
    //primero quito la funcion click y reseteo los valores 
    //si misma id de imagen, entonces iguales y se dejan abiertos o se puede ocultar
    if (listaObjetosCelda[numA].id === listaObjetosCelda[numB].id) {//si els objectes tienen el mateix id de imatge, llavors son iguals
        msg = "iguales";
        ++contador;//incrementa el contador
        showContador(contador);//pinto per pantalla el contador
        if (contador === 6) {
            msg = "Ganaste!";
            winnerMessage(msg);// si guanya
        } else {
            setTimeout(resetVals, 1000);
        }


    } else {//si son diferentes, se vuelve a ocultar
        msg = "diferentes";
        setTimeout(voltearCeldas, 1000);//si els canvas tenen imatges diferents, llavors es tornan a girar.
    }
    showMsg(msg);//mpstrem algun missatge
}

function showMsg(msg) {
    document.getElementById('msg').innerHTML = "<p class=mensajeTxt>" + msg;
}

function winnerMessage(msg) {
    document.getElementById('titol').innerHTML = "<p class=mensajeTxt> Felicidades " + msg;
}

function showContador(msg) {
    document.getElementById('num').innerHTML = "<p class=mensajeTxt>" + msg;
}

/**
 * Mètode per tornar a girar els canvas que no son iguals
 * @returns {undefined}
 */
function voltearCeldas() {
    //alert("restaurar fondo");
    var canvas1 = "canvas" + (numA + 1);
    var canvas2 = "canvas" + (numB + 1);
    var url = "images/fondo_canvas.png";//imatge per defecte
    var canA = document.getElementById(canvas1);
    canA.setAttribute("onclick", "canvasClicked(" + (numA + 1) + ")");//torno a activar el mètode onclick
    cargarImagen(canA, url);
    var canB = document.getElementById(canvas2);
    canB.setAttribute("onclick", "canvasClicked(" + (numB + 1) + ")");//torno a activar el mètode onclick
    cargarImagen(canB, url);
    resetVals();//reseteo els valors
}

/**
 * Mètode per tornar a inicialitzar les variables auxiliars
 * @returns {undefined}
 */
function resetVals() {
    canvasA = canvasB = false;
    numA = numB = -1;
    showMsg("¿?");
}



/**
 * Mètode per tornar a carregar la pàgina
 * @returns {undefined}
 */
function reiniciar() {
    location.reload(true);
}

/**
 * Objecte cel.la
 * @param {type} id ID de la imatge
 * @param {type} img Url de la imatge
 * @returns {Celda}
 */
function Celda(id, img) {
    this.id = id;
    this.imagen = img;
    this.estado = false;
}